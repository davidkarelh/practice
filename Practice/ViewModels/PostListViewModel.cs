﻿using Practice.Models;
using System.ComponentModel.DataAnnotations;

namespace Practice.ViewModels
{
    public class PostListViewModel
    {
        public PostListViewModel(Post post, string UserName)
        {
            Title = post.Title;
            Slug = post.Slug;
            this.UserName = UserName;
            IdPost = post.IdPost;
        }
        public string Title { get; set; }
        public string Slug { get; set; }
        [Display(Name = "Username")]
        public string UserName { get; set; }
        public int IdPost { get; set; }
    }
}

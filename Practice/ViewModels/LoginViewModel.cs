﻿using Microsoft.AspNetCore.Identity;
using Practice.Models;
using System.ComponentModel.DataAnnotations;

namespace Practice.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

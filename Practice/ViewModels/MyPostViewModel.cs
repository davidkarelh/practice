﻿using Practice.Models;
using System.ComponentModel.DataAnnotations;

namespace Practice.ViewModels
{
    public class MyPostViewModel
    {
        public MyPostViewModel(Post post)
        {
            Title = post.Title;
            Slug = post.Slug;
            IdPost = post.IdPost;
        }
        public string Title { get; set; }
        public string Slug { get; set; }
        public int IdPost { get; set; }
    }
}

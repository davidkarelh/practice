﻿using Practice.Models;
using System.ComponentModel.DataAnnotations;

namespace Practice.ViewModels
{
    public class UserDetailViewModel
    {
        public UserDetailViewModel(User user)
        {
            FullName = user.FullName;
            UserName = user.UserName;
            Email = user.Email;
            BirthDate = user.BirthDate.ToLongDateString();
            PhoneNumber = user.PhoneNumber;
        }
        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        [Display(Name = "Username")]
        public string UserName { get; set; }
        public string Email { get; set; }
        [Display(Name = "Birth Date")]
        public string BirthDate { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

    }
}

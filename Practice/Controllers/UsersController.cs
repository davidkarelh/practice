﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Practice.Models;
using Practice.ViewModels;

namespace Practice.Controllers
{
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UsersController(ApplicationDbContext context)
        {
            _context = context;
        }

        public  IActionResult Index()
        {
            return _context.Users != null ?
                    View(_context.Users.ToList()) :
                    Problem("Entity set 'ApplicationDbContext.Posts'  is null.");
        }

        public IActionResult Details(string? id)
        {
            if (id == null || _context.Users == null)
            {
                return NotFound();
            }

            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(new UserDetailViewModel(user));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Practice.Models;
using Practice.ViewModels;

namespace Practice.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> userManager;

        public PostsController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        // GET: Posts
        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            var posts = await _context.Posts.ToListAsync();
            var users = await _context.Users.ToListAsync();
            if (users == null)
            {
                return NotFound();
            }
            var viewPosts = posts.Where(p => p.UserId != null).Select(p => new PostListViewModel(p, users.Where(u => u.Id != null).FirstOrDefault(u => u.Id == p.UserId).UserName)).ToList();
            if (viewPosts == null)
            {
                return NotFound();
            }
            return View(viewPosts);
        }
        [AllowAnonymous]
        public async Task<IActionResult> UserPosts(string id)
        {
            var posts = await _context.Posts.ToListAsync();
            var user = await userManager.FindByIdAsync(id);
            var viewPosts = posts.Where(p => p != null).Where(p => p.User != null).Where(p => p.User.Equals(user)).Select(p => new PostListViewModel(p, user.UserName)).ToList();
            return View("Index", viewPosts);
        }

        public async Task<IActionResult> MyPosts()
        {
            var posts = await _context.Posts.ToListAsync();
            var user = await userManager.GetUserAsync(User);
            var viewPosts = posts.Where(p => p != null).Where(p => p.User != null).Where(p => p.User.Equals(user)).Select(p => new MyPostViewModel(p)).ToList();
            return View("MyPosts", viewPosts);

        }

        // GET: Posts/Details/5
        [AllowAnonymous]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Posts == null)
            {
                return NotFound();
            }

            var post = await _context.Posts
                .FirstOrDefaultAsync(m => m.IdPost == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Posts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPost,Title,Slug,Content")] Post post)
        {
            if (ModelState.IsValid)
            {
                post.User = await userManager.GetUserAsync(User);
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(MyPosts));
            }
            return View(post);
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Posts == null)
            {
                return NotFound();
            }

            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            var currentUser = await userManager.GetUserAsync(User);
            if (!(post.UserId).Equals(currentUser.Id))
            {
                return NotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPost,UserId,Title,Slug,Content")] Post post)
        {
            if (id != post.IdPost)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var currentUser = await userManager.GetUserAsync(User);
                    if (!(post.UserId).Equals(currentUser.Id))
                    {
                        return NotFound();
                    }
                    post.User = currentUser;
                    _context.Update(post);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(post.IdPost))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(MyPosts));
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Posts == null)
            {
                return NotFound();
            }

            var post = await _context.Posts
                .FirstOrDefaultAsync(m => m.IdPost == id);
            if (post == null)
            {
                return NotFound();
            }
            var currentUser = await userManager.GetUserAsync(User);
            if (!(post.UserId).Equals(currentUser.Id))
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Posts == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Posts'  is null.");
            }
            var post = await _context.Posts.FindAsync(id);

            if (post != null)
            {
                var currentUser = await userManager.GetUserAsync(User);
                if (!(post.UserId).Equals(currentUser.Id))
                {
                    return NotFound();
                }
                _context.Posts.Remove(post);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(MyPosts));
        }

        private bool PostExists(int id)
        {
          return (_context.Posts?.Any(e => e.IdPost == id)).GetValueOrDefault();
        }
    }
}

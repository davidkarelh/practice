﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Practice.Models
{
    public class User : IdentityUser
    {
        [Required]
        [Display(Name = "Full Name")]
        public string? FullName { get; set; }
        [Required]
        [Column(TypeName = "Date")]
        [Display(Name = "Birth Date")]
        public DateTime BirthDate { get; set; }

    }
}

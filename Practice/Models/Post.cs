﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;

namespace Practice.Models
{
    public class Post
    {
        [Key]
        public int IdPost { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Slug { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        [ValidateNever]
        public string UserId { get; set; }
        [Required]
        [ValidateNever]
        public User User { get; set; }

    }
}
